Exercise Sheet Oganisation via git
===============================

- In `main.tex` Comment in and out the line `\solntrue` to produce pdfs with and without solutions.

- `main-auditorium.tex` is for the overhead projector in big lecture rooms (like Fo2). 
	You have to scale the page in printer options.

- `main-handnotes.tex` is for handnotes.

- `main-tutor-overhead.tex` is for the Tutors who use the overhead projector.
